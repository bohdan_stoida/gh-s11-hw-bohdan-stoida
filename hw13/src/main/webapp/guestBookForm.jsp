<%@ page import="org.geekhub.bohdan.UserFeedback" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Guest book</title>
</head>
<body>
<h1>Guest book form</h1>
<form action="/guest-book" method="post">
    <p>Name <input type="text" name="name"></p>
    <p>Message <textarea rows="10" cols="45" name="text"></textarea></p>
    <p>Rating <select name="Rating">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select></p>
    <p>Submit changes <input type="submit" value="Save"></p>
</form>
<p>
    <% ArrayList<UserFeedback> userFeedbackList = (ArrayList) request.getAttribute("userFeedbackList"); %>
    <%
        if (userFeedbackList != null) {
            for (UserFeedback userFeedback : userFeedbackList) {
                out.println("<p>" + "<h3>" + userFeedback.getUserName() + "<br>"
                        + userFeedback.getFeedBackMessage() + "<br>" +
                        userFeedback.getRating() + "<br>" +
                        userFeedback.getDate() + "</h3>" + "</p>");
            }
        } else {
            out.println("Feedback list is empty right now!");
        }
    %>
</p>
</body>
</html>