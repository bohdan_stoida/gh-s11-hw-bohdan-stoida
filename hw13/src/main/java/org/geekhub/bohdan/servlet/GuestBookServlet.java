package org.geekhub.bohdan.servlet;

import org.geekhub.bohdan.UserFeedback;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.*;

public class GuestBookServlet extends HttpServlet {
    private final List<UserFeedback> userFeedbackList = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String message = request.getParameter("text");
        String rating = request.getParameter("Rating");

        PrintWriter printWriter = response.getWriter();
        printWriter.println("Name: " + name + "\nMessage: " + message + "\nRating: " + rating);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        String message = request.getParameter("text");
        Integer rating = Integer.parseInt(request.getParameter("Rating"));

        Random random = new Random();
        userFeedbackList.add(new UserFeedback(name, message, rating, LocalDateTime.of(2022,
                getRandomNumberUsingNextInt(1, 12),
                getRandomNumberUsingNextInt(1, 28),
                random.nextInt(24),
                random.nextInt(60))));

        Collections.sort(userFeedbackList, new Comparator<UserFeedback>() {
            @Override
            public int compare(UserFeedback o1, UserFeedback o2) {
                return o2.getDate().compareTo(o1.getDate());
            }
        });

        request.setAttribute("userFeedbackList", userFeedbackList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/guest-book-form-jsp");
        dispatcher.forward(request, response);
    }

    public int getRandomNumberUsingNextInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }
}