package org.geekhub.bohdan;

import java.time.LocalDateTime;
import java.util.Comparator;

public class UserFeedback implements Comparator<UserFeedback> {
    private String userName;
    private String feedBackMessage;
    private Integer rating;
    private LocalDateTime date;

    public UserFeedback(String userName, String feedBackMessage, Integer rating, LocalDateTime date) {
        this.userName = userName;
        this.feedBackMessage = feedBackMessage;
        this.rating = rating;
        this.date = date;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFeedBackMessage() {
        return feedBackMessage;
    }

    public void setFeedBackMessage(String feedBackMessage) {
        this.feedBackMessage = feedBackMessage;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public int compare(UserFeedback o1, UserFeedback o2) {
        return o1.getDate().getMonthValue() > o2.getDate().getMonthValue() ? -1 : 1;
    }
}
