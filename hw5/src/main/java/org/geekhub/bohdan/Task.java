package org.geekhub.bohdan;

import java.util.Objects;

public class Task {

    private final String name;
    private final String category;

    public Task(String name, String category) {
        this.name = name;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(name, task.name) && Objects.equals(category, task.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, category);
    }
}
