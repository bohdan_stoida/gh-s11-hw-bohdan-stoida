package org.geekhub.bohdan;

import java.time.LocalDateTime;
import java.util.*;

public class TaskManager implements ITaskManager {

    private final List<Task> taskList = new ArrayList<>();
    private final TreeMap<LocalDateTime, List<Task>> timeListTreeMap = new TreeMap<>();
    private final Map<String, List<Task>> stringListMap = new HashMap<>();
    private final Set<String> stringSet = new HashSet<>();

    public TaskManager() {
    }

    @Override
    public void add(LocalDateTime date, Task task) {
        taskList.add(task);
        stringSet.add(task.getCategory());
        timeListTreeMap.put(date, taskList);
    }

    @Override
    public void remove(LocalDateTime date) {
        System.out.println(timeListTreeMap);
        timeListTreeMap.remove(date);
        System.out.println(timeListTreeMap);
    }

    @Override
    public Set<String> getCategories() {
        return stringSet;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories(String... categories) {
        for (String category : categories) {
            stringListMap.put(category, getTasksByCategory(category));
        }

        return stringListMap;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> sortedTasks = getTasksForToday();
        List<Task> tasksByCategory = new ArrayList<>();

        for (Task sortedTask : sortedTasks) {
            if (Objects.equals(category, sortedTask.getCategory())) {
                tasksByCategory.add(sortedTask);
            }
        }

        return tasksByCategory;
    }

    @Override
    public List<Task> getTasksForToday() {
        LocalDateTime dateTime = (LocalDateTime) timeListTreeMap.keySet().toArray()[0];

        return timeListTreeMap.get(dateTime);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskManager that = (TaskManager) o;
        return Objects.equals(taskList, that.taskList) && Objects.equals(timeListTreeMap, that.timeListTreeMap) && Objects.equals(stringListMap, that.stringListMap) && Objects.equals(stringSet, that.stringSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskList, timeListTreeMap, stringListMap, stringSet);
    }
}
