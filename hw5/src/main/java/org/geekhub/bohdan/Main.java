package org.geekhub.bohdan;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        TaskManager taskManager = new TaskManager();
        LocalDateTime dateTime = LocalDateTime.of(2021, Month.DECEMBER, 29, 12, 0);

        taskManager.add(dateTime, new Task("Wash the dishes", "Home work"));
        taskManager.add(dateTime.plusHours(1), new Task("Vacuum", "Home work"));
        taskManager.add(dateTime.plusHours(2), new Task("Cook food", "Home work"));
        taskManager.add(dateTime.plusHours(3), new Task("Learn English", "Study"));
        taskManager.add(dateTime.plusHours(4), new Task("Learn Java", "Study"));
        taskManager.add(dateTime.plusHours(5), new Task("Workout", "Health"));


        System.out.println("Tasks for today:");
        List<Task> tasks = taskManager.getTasksForToday();
        for (Task task : tasks) {
            System.out.println(task.getName() + " - " + task.getCategory());
        }

        System.out.println("\nTask categories:");
        for (String category : taskManager.getCategories()) {
            System.out.println(category);
        }

        System.out.println("\nTasks by category:");
        for (Task taskByCategory : taskManager.getTasksByCategory("Study")) {
            System.out.println(taskByCategory.getName() + " - " + taskByCategory.getCategory());
        }

        System.out.println("\nTasks by categories:");
        Map<String, List<Task>> map = taskManager.getTasksByCategories("Home work", "Health");
        List<Task> tasksByCategories = new ArrayList<>();
        for (String key : map.keySet()) {
            for (Task task : tasks) {
                if (Objects.equals(task.getCategory(), key))
                    tasksByCategories.add(task);
            }
        }
        for (Task tasksByCategory : tasksByCategories) {
            System.out.println(tasksByCategory.getName() + " - " + tasksByCategory.getCategory());
        }

        taskManager.remove(dateTime);
    }
}
