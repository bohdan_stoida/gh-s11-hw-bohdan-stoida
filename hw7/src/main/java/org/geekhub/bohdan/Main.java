package org.geekhub.bohdan;

public class Main {

    public static void main(String[] args) {

        String path = "TestFilesForHw7";
        ZipFileMaker zipFileMaker = new ZipFileMaker(path);
        zipFileMaker.readFiles(zipFileMaker.getDir());
        System.out.println("\n-----------------");
        zipFileMaker.zipImages();
        zipFileMaker.zipAudios();
        zipFileMaker.zipVideos();
    }
}
