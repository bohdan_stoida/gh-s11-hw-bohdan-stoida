package org.geekhub.bohdan;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFileMaker {

    private final File dir;
    private final List<File> fileList = new ArrayList<>();

    private final FileFilter filterImages = file -> file.getName().endsWith(".jpg") || file.getName().endsWith(".png") ||
            file.getName().endsWith(".gif") || file.getName().endsWith(".jpeg");
    private final FileFilter filterAudios = file -> file.getName().endsWith(".wav") || file.getName().endsWith(".mp3") ||
            file.getName().endsWith(".wma");
    private final FileFilter filterVideos = file -> file.getName().endsWith(".avi") || file.getName().endsWith(".mp4") ||
            file.getName().endsWith(".flv");

    public ZipFileMaker(String path) {
        dir = new File(path);
    }

    public void readFiles(File baseDir) {
        if (baseDir != null)
            if (baseDir.isDirectory()) {
                for (File file : baseDir.listFiles()) {
                    if (file.isFile()) {
                        fileList.add(file);
                        System.out.println(file.getName() + " file added");
                    } else {
                        System.out.println(file.getName() + " directory");
                        readFiles(file);
                    }
                }
            }
    }

    private void getFileList(List<File> files, FileFilter fileFilter) {
        for (File file : fileList) {
            if (fileFilter.accept(file)) {
                files.add(file);
                System.out.println(file.getName());
            }
        }
    }

    public void zipImages() {
        zippingProcess(filterImages, "images");
    }

    public void zipAudios() {
        zippingProcess(filterAudios, "audios");
    }

    public void zipVideos() {
        zippingProcess(filterVideos, "videos");
    }

    private void zippingProcess(FileFilter filterVideos, String archiveName) {
        List<File> fileList = new ArrayList<>();
        getFileList(fileList, filterVideos);

        System.out.println("Zipping process started!");
        ZipOutputStream zipImages = null;
        try {
            zipImages = new ZipOutputStream(new FileOutputStream("TestFilesForHw7\\" + archiveName + ".zip"));
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        for (File file : fileList) {
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                ZipEntry imageEntry = new ZipEntry(file.getName());
                zipImages.putNextEntry(imageEntry);
                byte[] bytes = new byte[fileInputStream.available()];
                fileInputStream.read(bytes);
                zipImages.write(bytes);
                zipImages.closeEntry();
                System.out.println(file.getName() + " zipped...");
            } catch (IOException | NullPointerException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
        try {
            zipImages.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        System.out.println("\n----------------");
    }

    public File getDir() {
        return dir;
    }
}
