package org.geekhub.bohdan.task2;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

public class Task {

    private int id;
    private TaskType type;
    private String title;
    private boolean done;
    private Set<String> categories;
    private LocalDate startsOn;

    public Task(int id, TaskType type, String title, boolean done, Set<String> categories, LocalDate startsOn) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.done = done;
        this.categories = categories;
        this.startsOn = startsOn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Set<String> getCategories() {
        return categories;
    }

    public void setCategories(Set<String> categories) {
        this.categories = categories;
    }

    public LocalDate getStartsOn() {
        return startsOn;
    }

    public void setStartsOn(LocalDate startsOn) {
        this.startsOn = startsOn;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", done=" + done +
                ", categories=" + categories +
                ", startsOn=" + startsOn +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id && done == task.done && type == task.type && Objects.equals(title, task.title) && Objects.equals(categories, task.categories) && Objects.equals(startsOn, task.startsOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, title, done, categories, startsOn);
    }
}
