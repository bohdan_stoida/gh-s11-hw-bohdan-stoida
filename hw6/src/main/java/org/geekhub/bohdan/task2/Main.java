package org.geekhub.bohdan.task2;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        TaskManagerImpl taskManager = new TaskManagerImpl();
        Set<String> firstCategories = new HashSet<>();
        firstCategories.add("Buy products");
        firstCategories.add("Cook the food");
        firstCategories.add("Eat the food");
        Set<String> secondCategories = new HashSet<>();
        secondCategories.add("Start sleeping");
        Set<String> thirdCategories = new HashSet<>();
        for (int i = 0; i < 6; i++)
            thirdCategories.add("category" + i);
        Set<String> fourthCategories = new HashSet<>();
        for (int i = 0; i < 2; i++)
            fourthCategories.add("category" + i);
        Set<String> fifthCategories = new HashSet<>();
        for (int i = 0; i < 1; i++)
            fifthCategories.add("category" + i);
        Set<String> sixthCategories = new HashSet<>();
        for (int i = 0; i < 3; i++)
            sixthCategories.add("category" + i);
        Set<String> seventhCategories = new HashSet<>();
        for (int i = 0; i < 4; i++)
            seventhCategories.add("category" + i);
        Set<String> eighthCategories = new HashSet<>();
        for (int i = 0; i < 5; i++)
            eighthCategories.add("category" + i);
        Set<String> ninthCategories = new HashSet<>();
        for (int i = 0; i < 2; i++)
            ninthCategories.add("category" + i);
        Set<String> tenthCategories = new HashSet<>();
        for (int i = 0; i < 7; i++)
            tenthCategories.add("category" + i);
        Set<String> eleventhCategories = new HashSet<>();
        for (int i = 0; i < 4; i++)
            eleventhCategories.add("category" + i);
        Set<String> twelfthCategories = new HashSet<>();
        for (int i = 0; i < 1; i++)
            twelfthCategories.add("category" + i);

        taskManager.addTask(new Task(0, TaskType.IMPORTANT, "Eat", false, firstCategories, LocalDate.of(2022, Month.JANUARY, 29)));
        taskManager.addTask(new Task(1, TaskType.IMPORTANT, "Sleep", true, secondCategories, LocalDate.of(2022, Month.JANUARY, 19)));
        taskManager.addTask(new Task(2, TaskType.UNIMPORTANT, "Run", true, thirdCategories, LocalDate.of(2022, Month.JANUARY, 20)));
        taskManager.addTask(new Task(3, TaskType.IMPORTANT, "Learn", false, fourthCategories, LocalDate.of(2022, Month.JANUARY, 21)));
        taskManager.addTask(new Task(4, TaskType.IMPORTANT, "Swim", false, fifthCategories, LocalDate.of(2022, Month.DECEMBER, 20)));
        taskManager.addTask(new Task(5, TaskType.IMPORTANT, "Read", false, sixthCategories, LocalDate.of(2022, Month.DECEMBER, 21)));
        taskManager.addTask(new Task(6, TaskType.IMPORTANT, "Paint", false, seventhCategories, LocalDate.of(2022, Month.DECEMBER, 21)));
        taskManager.addTask(new Task(7, TaskType.UNIMPORTANT, "Learn java", false, eighthCategories, LocalDate.of(2021, Month.DECEMBER, 29)));
        taskManager.addTask(new Task(8, TaskType.IMPORTANT, "Learn C#", true, ninthCategories, LocalDate.of(2021, Month.DECEMBER, 29)));
        taskManager.addTask(new Task(9, TaskType.IMPORTANT, "Go to university", false, tenthCategories, LocalDate.of(2021, Month.DECEMBER, 30)));
        taskManager.addTask(new Task(10, TaskType.IMPORTANT, "Shopping", false, eleventhCategories, LocalDate.of(2021, Month.DECEMBER, 30)));
        taskManager.addTask(new Task(11, TaskType.IMPORTANT, "Go for a walk", false, twelfthCategories, LocalDate.of(2021, Month.DECEMBER, 30)));


        List<Task> fiveNearestImportantTasks = taskManager.find5NearestImportantTasks(taskManager.getTaskList());
        System.out.println("Important tasks");
        for (Task fiveNearestImportantTask : fiveNearestImportantTasks) {
            System.out.println(fiveNearestImportantTask);
        }

        List<String> uniqueCategories = taskManager.getUniqueCategories(taskManager.getTaskList());
        System.out.println("\nUnique categories");
        for (String uniqueCategory : uniqueCategories) {
            System.out.println(uniqueCategory);
        }

        System.out.println("\nCategories with tasks");
        Map<String, List<Task>> taskMap = taskManager.getCategoriesWithTasks(taskManager.getTaskList());
        taskMap.keySet().forEach(System.out::println);
        for (String s : taskMap.keySet()) {
            System.out.println(taskMap.get(s));
        }

        System.out.println("\nList of done and in progress tasks");
        Map<Boolean, List<Task>> booleanListMap = taskManager.splitTasksIntoDoneAndInProgress(taskManager.getTaskList());
        for (Boolean aBoolean : booleanListMap.keySet()) {
            System.out.println(booleanListMap.get(aBoolean));
        }

        System.out.println("\nTasks present or no?");
        boolean isPresent = taskManager.existsTaskOfCategory(taskManager.getTaskList(), "Start sleeping");
        System.out.println("Tasks is present - " + isPresent);

        System.out.println("\nTasks titles range joined by comma");
        String titlesRange = taskManager.getTitlesOfTasks(taskManager.getTaskList(), 2, 7);
        System.out.println(titlesRange);

        System.out.println("\nCount of tasks in each category");
        Map<String, Long> countByCategoryMap = taskManager.getCountsByCategories(taskManager.getTaskList());
        for (String s : countByCategoryMap.keySet()) {
            System.out.println(countByCategoryMap.get(s));
        }

        System.out.println("\nStats about lengths of categories names");
        IntSummaryStatistics summaryStatistics = taskManager.getCategoriesNamesLengthStatistics(taskManager.getTaskList());
        System.out.println("Max value: " + summaryStatistics.getMax() +
                "\nMin value: " + summaryStatistics.getMin() +
                "\nValue count: " + summaryStatistics.getCount() +
                "\nValue average: " + summaryStatistics.getAverage() +
                "\nValue sum: " + summaryStatistics.getSum());

        System.out.println("\nTask that has longest categories set");
        Task theBiggestTask = taskManager.findTaskWithBiggestCountOfCategories(taskManager.getTaskList());
        System.out.println(theBiggestTask);
    }
}
