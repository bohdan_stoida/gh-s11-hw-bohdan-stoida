package org.geekhub.bohdan.task2;

public enum TaskType {

    IMPORTANT,
    UNIMPORTANT
}
