package org.geekhub.bohdan.task2;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;

public class TaskManagerImpl implements TaskManager {

    private final List<Task> taskList = new ArrayList<>();

    public TaskManagerImpl() {
    }

    public void addTask(Task task) {
        taskList.add(task);
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    @Override
    public List<Task> find5NearestImportantTasks(List<Task> tasks) {
        return tasks.stream()
                .filter(task -> task.getType().equals(TaskType.IMPORTANT))
                .filter(task -> task.getStartsOn().isBefore(LocalDate.of(2022, Month.JANUARY, 31)))
                .filter(task -> !task.isDone())
                .limit(5)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
                .flatMap(task -> task.getCategories()
                        .stream()
                        .map(Object::toString))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors
                        .groupingBy(task -> task.getCategories()
                                .stream()
                                .findAny()
                                .orElse(null)));
    }

    @Override
    public Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors
                        .groupingBy(Task::isDone));
    }

    @Override
    public boolean existsTaskOfCategory(List<Task> tasks, String category) {
        return tasks.stream().anyMatch(task -> task.getCategories().contains(category));
    }

    @Override
    public String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream()
                .filter(task -> task.getId() >= startNo && task.getId() <= endNo)
                .map(Task::getTitle)
                .collect(Collectors.joining(", "));
    }

    @Override
    public Map<String, Long> getCountsByCategories(List<Task> tasks) {
        return tasks.stream()
                .map(Task::getCategories)
                .map(Set::iterator)
                .collect(Collectors.
                        groupingBy(Iterator::next, Collectors.counting()));
    }

    @Override
    public IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return tasks.stream()
                .map(Task::getCategories)
                .map(Set::size)
                .collect(IntSummaryStatistics::new, IntSummaryStatistics::accept, IntSummaryStatistics::combine);
    }

    @Override
    public Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
        return tasks.stream()
                .max(Comparator.comparing(task -> task.getCategories().size()))
                .orElse(null);
    }
}
