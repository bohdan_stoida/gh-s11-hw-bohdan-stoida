package org.geekhub.bohdan.task1;

import java.util.Comparator;
import java.util.function.Supplier;

public interface List<E> {

    void addLast(E e);

    void exchangeTwoElements(E firstElement, E secondElement);

    LinkedList<E> bubbleSort(Comparator<LinkedList<Integer>> comparator, Direction direction, Supplier<LinkedList<Integer>> supplier);

    LinkedList<E> insertionSort(Comparator<LinkedList<Integer>> comparator, Direction direction, Supplier<LinkedList<Integer>> supplier);
}
