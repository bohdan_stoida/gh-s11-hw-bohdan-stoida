package org.geekhub.bohdan.task1;

import java.util.Comparator;
import java.util.function.Supplier;

public class Main {

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        list.addLast(5);
        list.addLast(4);
        list.addLast(7);
        list.addLast(1);
        list.addLast(9);
        list.showElements();
        System.out.println("Exchanging elements\n");
        list.exchangeTwoElements(5, 1);
        list.showElements();
        System.out.println("Bubble sort ASC\n");
        LinkedList<Integer> bubbleSorted = list.bubbleSort(new Comparator<LinkedList<Integer>>() {
            @Override
            public int compare(LinkedList<Integer> o1, LinkedList<Integer> o2) {
                return o2.getFirst().getItem() - o1.getLast().getItem();
            }
        }, Direction.ASC, new Supplier<LinkedList<Integer>>() {
            @Override
            public LinkedList<Integer> get() {
                return list;
            }
        });
        bubbleSorted.showElements();
        System.out.println("Bubble sort DESC\n");
        bubbleSorted = list.bubbleSort(new Comparator<LinkedList<Integer>>() {
            @Override
            public int compare(LinkedList<Integer> o1, LinkedList<Integer> o2) {
                return o1.getFirst().getItem() - o2.getLast().getItem();
            }
        }, Direction.DESC, new Supplier<LinkedList<Integer>>() {
            @Override
            public LinkedList<Integer> get() {
                return list;
            }
        });
        bubbleSorted.showElements();
        System.out.println("Insertion sort ASC\n");
        LinkedList<Integer> insertionSorted = list.insertionSort(new Comparator<LinkedList<Integer>>() {
            @Override
            public int compare(LinkedList<Integer> o1, LinkedList<Integer> o2) {
                return o2.getFirst().getItem() - o1.getLast().getItem();
            }
        }, Direction.ASC, new Supplier<LinkedList<Integer>>() {
            @Override
            public LinkedList<Integer> get() {
                return list;
            }
        });

        insertionSorted.showElements();

        System.out.println("Insertion sort DESC\n");
        insertionSorted = list.insertionSort(new Comparator<LinkedList<Integer>>() {
            @Override
            public int compare(LinkedList<Integer> o1, LinkedList<Integer> o2) {
                return o1.getFirst().getItem() - o2.getLast().getItem();
            }
        }, Direction.DESC, new Supplier<LinkedList<Integer>>() {
            @Override
            public LinkedList<Integer> get() {
                return list;
            }
        });

        insertionSorted.showElements();
    }
}
