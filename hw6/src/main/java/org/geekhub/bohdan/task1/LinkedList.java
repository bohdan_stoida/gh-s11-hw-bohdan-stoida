package org.geekhub.bohdan.task1;

import java.util.Comparator;
import java.util.function.Supplier;

public class LinkedList<E> implements List<E> {

    private int size = 0;
    private Node<E> first;
    private Node<E> last;

    public LinkedList() {
        first = null;
        last = null;
    }

    @Override
    public void addLast(E e) {
        Node<E> tempNode = new Node<>(e);
        if (first == null) {
            first = tempNode;
        } else {
            last.next = tempNode;
        }
        tempNode.prev = last;
        last = tempNode;
        size++;
    }

    public void showElements() {
        Node<E> tempNode = first;

        while (tempNode != null) {
            System.out.println(tempNode.item);
            tempNode = tempNode.next;
        }
    }

    public void addElByIndex(E element, int index) {
        Node<E> currentNode = first;
        int count = 0;
        while (currentNode != null && count != index) {
            currentNode = currentNode.next;
            count++;
        }
        Node<E> tempNode = new Node<>(element);
        currentNode.prev.next = tempNode;
        tempNode.prev = currentNode.prev;
        currentNode.prev = tempNode;
        tempNode.next = currentNode;
    }

    public E getIndex(int index) {
        Node<E> currentNode = first;
        int count = 0;
        while (currentNode != null && count != index) {
            currentNode = currentNode.next;
            count++;
        }

        return currentNode.item;
    }

    @Override
    public void exchangeTwoElements(E firstElement, E secondElement) {
        boolean firstFound = false;
        boolean secondFound = false;
        Node<E> node = first;
        while (node != null && !(firstFound && secondFound)) {
            if (node.item == firstElement) {
                node.item = secondElement;
                firstFound = true;
                node = node.next;
                continue;
            }
            if (node.item == secondElement) {
                node.item = firstElement;
                secondFound = true;
                node = node.next;
                continue;
            }
            node = node.next;
        }
    }

    @Override
    public LinkedList<E> bubbleSort(Comparator<LinkedList<Integer>> comparator, Direction direction, Supplier<LinkedList<Integer>> supplier) {
        switch (direction) {
            case ASC:
                boolean isSorted = false;
                int tempVar;

                while (!isSorted) {
                    isSorted = true;

                    for (int i = 0; i < supplier.get().size - 1; i++) {
                        if (supplier.get().getIndex(i) > supplier.get().getIndex(i + 1)) {
                            tempVar = supplier.get().getIndex(i);
                            supplier.get().addElByIndex(supplier.get().getIndex(i + 1), i);
                            supplier.get().addElByIndex(tempVar, i + 1);
                            isSorted = false;
                        }
                    }
                }
                break;
            case DESC:
                Node<Integer> currentNode;
                Node<Integer> newFirstNode = null;
                Node<Integer> moveNode;
                Node<Integer> prev;

                while (supplier.get().first != null) {
                    prev = null;
                    currentNode = supplier.get().first;
                    moveNode = supplier.get().first;
                    while (currentNode != null) {
                        if (currentNode.next != null && (int) (currentNode.next.item) < (int) moveNode.item) {
                            moveNode = currentNode.next;
                            prev = currentNode;
                        }
                        currentNode = currentNode.next;
                    }
                    if (moveNode == supplier.get().first)
                        supplier.get().first = supplier.get().first.next;
                    if (prev != null)
                        prev.next = moveNode.next;
                    moveNode.next = newFirstNode;
                    newFirstNode = moveNode;
                }
                supplier.get().first = newFirstNode;
                break;
        }

        return this;
    }

    @Override
    public LinkedList<E> insertionSort(Comparator<LinkedList<Integer>> comparator, Direction direction, Supplier<LinkedList<Integer>> supplier) {

        switch (direction) {
            case ASC:
                Node<Integer> sorted = null;
                Node<Integer> current = supplier.get().first;
                while (current != null) {
                    Node<Integer> next = current.next;
                    sorted = insertionSortedAsc(current, sorted);
                    current = next;
                }
                supplier.get().first = sorted;
                break;
            case DESC:
                sorted = null;
                current = supplier.get().first;
                while (current != null) {
                    Node<Integer> next = current.next;
                    sorted = insertionSortedDesc(current, sorted);
                    current = next;
                }
                supplier.get().first = sorted;
                break;
        }

        return this;
    }

    Node<Integer> insertionSortedAsc(Node<Integer> newNode, Node<Integer> sorted) {
        if (sorted == null || sorted.item >= newNode.item) {
            newNode.next = sorted;
            sorted = newNode;
        } else {
            Node<Integer> current = sorted;
            while (current.next != null && current.next.item < newNode.item)
                current = current.next;
            newNode.next = current.next;
            current.next = newNode;
        }

        return sorted;
    }

    Node<Integer> insertionSortedDesc(Node<Integer> newNode, Node<Integer> sorted) {
        if (sorted == null || sorted.item <= newNode.item) {
            newNode.next = sorted;
            sorted = newNode;
        } else {
            Node<Integer> current = sorted;
            while (current.next != null && current.next.item > newNode.item)
                current = current.next;
            newNode.next = current.next;
            current.next = newNode;
        }

        return sorted;
    }

    public int getSize() {
        return size;
    }

    public Node<E> getFirst() {
        return first;
    }

    public Node<E> getLast() {
        return last;
    }

    public static class Node<E> {

        private E item;
        private Node<E> next;
        private Node<E> prev;

        Node(E element) {
            this.item = element;
        }

        public E getItem() {
            return item;
        }

        public void setItem(E item) {
            this.item = item;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }
    }
}
