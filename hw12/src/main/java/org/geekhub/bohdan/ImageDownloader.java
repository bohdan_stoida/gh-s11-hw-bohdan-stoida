package org.geekhub.bohdan;

import java.io.*;
import java.net.URL;

public class ImageDownloader implements Runnable {
    private final String url;
    private final int imageCounter;

    public ImageDownloader(String url, int counter) {
        this.url = url;
        this.imageCounter = counter;
    }

    @Override
    public void run() {
        try(FileOutputStream fileOutputStream = new FileOutputStream("hw12/Images/image" + imageCounter + ".jpg");
            BufferedInputStream inputStream = new BufferedInputStream(new URL(this.url).openStream())) {
            byte data[] = new byte[1024];
            int length;
            while ((length = inputStream.read(data, 0, 1024)) != -1) {
                fileOutputStream.write(data, 0, length);
                fileOutputStream.flush();
            }
        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage());
        }
    }
}
