package org.geekhub.bohdan;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {

        Parser parser = new Parser("https://www.gettyimages.com/photos/travel?assettype=image&license=rf&alloweduse=availableforalluses&embeddable=true&family=creative&phrase=travel&sort=best");
        parser.parseImages();
        int imageCounter = 0;
        int numberOfThreads = 3;
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        for (String url : parser.getPicturesUrls()) {
            executorService.submit(new ImageDownloader(url, imageCounter++));
        }
        executorService.shutdown();
    }
}
