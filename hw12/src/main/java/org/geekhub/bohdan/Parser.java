package org.geekhub.bohdan;

import org.jsoup.Jsoup;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Parser {
    private final String url;
    private final List<String> picturesUrls = new ArrayList<>();

    public Parser(String url) {
        this.url = url;
    }

    public void parseImages() {
        try {
            int pageNumber = 1;
            int imgCount = 0;
            while (picturesUrls.size() != 200) {
                Document document = Jsoup.connect(url + "&page=" + pageNumber).userAgent("Chrome").get();
                Elements img = document.select("img");
                for (Element element : img) {
                    if (!element.attr("src").contains(".svg") && element.attr("src").length() != 0) {
                        imgCount++;
                        picturesUrls.add(element.attr("src"));
                        System.out.println("img #" + imgCount + " parsed. Page number - " + pageNumber);
                        if (picturesUrls.size() == 200) {
                            break;
                        }
                    }
                }
                pageNumber++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String picture : picturesUrls) {
            System.out.println(picture);
        }
        System.out.println("img count - " + picturesUrls.size());
    }

    public List<String> getPicturesUrls() {
        return picturesUrls;
    }
}
