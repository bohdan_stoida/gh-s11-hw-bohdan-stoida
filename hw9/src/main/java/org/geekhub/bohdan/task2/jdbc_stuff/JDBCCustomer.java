package org.geekhub.bohdan.task2.jdbc_stuff;

import org.geekhub.bohdan.task2.shop_stuff.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JDBCCustomer {
    private final Connection connection;
    private final List<Customer> customers;

    public JDBCCustomer(Connection connection, List<Customer> customers) {
        this.connection = connection;
        this.customers = customers;
    }

    public void insert() throws SQLException {
        String insertUpdCustomer = "INSERT INTO shop.public.customer (first_name, last_name, cell_phone) VALUES (?, ?, ?)"
                + "ON CONFLICT (id) DO UPDATE SET first_name = ?, last_name = ?, cell_phone = ?;";
        for (Customer customer : customers) {
            PreparedStatement preparedStatement = connection.prepareStatement(insertUpdCustomer);
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCellPhone());
            preparedStatement.setString(4, customer.getFirstName());
            preparedStatement.setString(5, customer.getLastName());
            preparedStatement.setString(6, customer.getCellPhone());
            preparedStatement.executeUpdate();
        }
    }

    public void delete(int customerId) throws SQLException {
        String deleteCustomer = "DELETE FROM shop.public.customer WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(deleteCustomer);
        preparedStatement.setInt(1, customerId);
        preparedStatement.executeUpdate();
    }

    public Map<Customer, Double> showTotalAmountOfMoney() throws SQLException {
        Map<Customer, Double> customerDoubleMap = new HashMap<>();

        String getCustomersMoney = "SELECT c.id, c.first_name, c.last_name, c.cell_phone, SUM(op.price) AS total_spent_money " +
                "FROM shop.public.customer c " +
                "JOIN shop.public.order o on c.id = o.customer " +
                "JOIN shop.public.ordering_product op on o.id = op.order " +
                "GROUP BY c.id;";

        PreparedStatement preparedStatement = connection.prepareStatement(getCustomersMoney);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String cellPhone = resultSet.getString("cell_phone");
            Double totalSpentMoney = resultSet.getDouble("total_spent_money");
            Customer customer = new Customer(firstName, lastName, cellPhone);
            customerDoubleMap.put(customer, totalSpentMoney);
        }

        return customerDoubleMap;
    }
}
