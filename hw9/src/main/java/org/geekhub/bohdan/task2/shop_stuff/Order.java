package org.geekhub.bohdan.task2.shop_stuff;

public class Order {
    private Customer customer;
    private OrderingProduct products;
    private String deliveryPlace;

    public Order(Customer customer, OrderingProduct products, String deliveryPlace) {
        this.customer = customer;
        this.products = products;
        this.deliveryPlace = deliveryPlace;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public OrderingProduct getProducts() {
        return products;
    }

    public void setProducts(OrderingProduct products) {
        this.products = products;
    }

    public String getDeliveryPlace() {
        return deliveryPlace;
    }

    public void setDeliveryPlace(String deliveryPlace) {
        this.deliveryPlace = deliveryPlace;
    }
}
