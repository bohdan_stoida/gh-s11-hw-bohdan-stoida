package org.geekhub.bohdan.task2;

import org.geekhub.bohdan.task2.jdbc_stuff.JDBCCustomer;
import org.geekhub.bohdan.task2.jdbc_stuff.JDBCOrder;
import org.geekhub.bohdan.task2.jdbc_stuff.JDBCProduct;
import org.geekhub.bohdan.task2.shop_stuff.Customer;
import org.geekhub.bohdan.task2.shop_stuff.Order;
import org.geekhub.bohdan.task2.shop_stuff.OrderingProduct;
import org.geekhub.bohdan.task2.shop_stuff.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws SQLException {
        String connUrl = "jdbc:postgresql://localhost:5432/shop";
        String userName = "postgres";
        String password = "spzabt_zz380";

        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("Eula", "Waiton", "393-237-8169"));
        customers.add(new Customer("Seymour", "Balstone", "233-891-6226"));
        customers.add(new Customer("Erhart", "Bainton", "737-763-9428"));
        customers.add(new Customer("Zorah", "Searby", "153-508-3785"));
        customers.add(new Customer("Jodi", "Monnoyer", "722-570-7880"));

        List<Product> products = new ArrayList<>();
        products.add(new Product("Creeping Treefern", "Cyathea armata (Sw.) Domin", 1.78));
        products.add(new Product("Deeproot Clubmoss", "Lycopodium tristachyum Pursh", 3.15));
        products.add(new Product("Rim Lichen", "Lecanora conizaeoides Nyl. ex Crombie", 2.65));
        products.add(new Product("Hybrid Oak", "Quercus ×palaeolithicola Trel.", 6.76));
        products.add(new Product("Sacramento Saltbush", "Atriplex persistens Stutz & G.L. Chu", 6.36));

        Random random = new Random();
        List<OrderingProduct> orderingProducts = new ArrayList<>();
        orderingProducts.add(new OrderingProduct(products.get(0), random.nextInt(100), 1.78));
        orderingProducts.add(new OrderingProduct(products.get(1), random.nextInt(100), 3.15));
        orderingProducts.add(new OrderingProduct(products.get(2), random.nextInt(100), 2.65));
        orderingProducts.add(new OrderingProduct(products.get(3), random.nextInt(100), 6.76));
        orderingProducts.add(new OrderingProduct(products.get(4), random.nextInt(100), 6.36));

        List<Order> orders = new ArrayList<>();
        orders.add(new Order(customers.get(0), orderingProducts.get(0), "Stare Miasto"));
        orders.add(new Order(customers.get(1), orderingProducts.get(1), "Nantes"));
        orders.add(new Order(customers.get(2), orderingProducts.get(2), "Vilarinho das Cambas"));
        orders.add(new Order(customers.get(3), orderingProducts.get(3), "Malapaubhara"));
        orders.add(new Order(customers.get(4), orderingProducts.get(4), "Frakulla e Madhe"));

        try (Connection connection = DriverManager.getConnection(connUrl, userName, password)) {
            PreparedStatement preparedStatement = connection.prepareStatement("TRUNCATE TABLE shop.public.customer RESTART IDENTITY CASCADE;");
            preparedStatement.executeUpdate();
            PreparedStatement preparedStatement1 = connection.prepareStatement("TRUNCATE TABLE shop.public.order RESTART IDENTITY CASCADE;");
            preparedStatement1.executeUpdate();
            PreparedStatement preparedStatement2 = connection.prepareStatement("TRUNCATE TABLE shop.public.ordering_product RESTART IDENTITY CASCADE;");
            preparedStatement2.executeUpdate();
            PreparedStatement preparedStatement3 = connection.prepareStatement("TRUNCATE TABLE shop.public.product RESTART IDENTITY CASCADE;");
            preparedStatement3.executeUpdate();

            JDBCCustomer jdbcCustomer = new JDBCCustomer(connection, customers);
            JDBCProduct jdbcProduct = new JDBCProduct(connection, products);
            JDBCOrder jdbcOrder = new JDBCOrder(connection, orders);

            jdbcCustomer.insert();
            jdbcProduct.insert();
            jdbcOrder.insert();

            Map<Customer, Double> customerDoubleMap = jdbcCustomer.showTotalAmountOfMoney();
            System.out.println("Total amount of spent money:");
            for (Customer customer : customerDoubleMap.keySet()) {
                System.out.println(customer + " - " + customerDoubleMap.get(customer) + "$");
            }

            System.out.println("The most popular product:");
            Map<Product, Integer> productIntegerMap = jdbcOrder.showMostPopularProduct();
            for (Product product : productIntegerMap.keySet()) {
                System.out.println(product + " - " + productIntegerMap.get(product));
            }
        }
    }
}
