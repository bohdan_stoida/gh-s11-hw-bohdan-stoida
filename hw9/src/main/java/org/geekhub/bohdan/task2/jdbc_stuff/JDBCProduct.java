package org.geekhub.bohdan.task2.jdbc_stuff;

import org.geekhub.bohdan.task2.shop_stuff.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class JDBCProduct {
    private final Connection connection;
    private final List<Product> products;

    public JDBCProduct(Connection connection, List<Product> customers) {
        this.connection = connection;
        this.products = customers;
    }

    public void insert() throws SQLException {
        String insertUpdProduct = "INSERT INTO shop.public.product (name, description, current_price) VALUES (?, ?, ?)"
                + "ON CONFLICT (id) DO UPDATE SET name = ?, description = ?, current_price = ?;";
        for (Product product : products) {
            PreparedStatement preparedStatement = connection.prepareStatement(insertUpdProduct);
            preparedStatement.setString(1, product.getName());
            preparedStatement.setString(2, product.getDescription());
            preparedStatement.setDouble(3, product.getCurrentPrice());
            preparedStatement.setString(4, product.getName());
            preparedStatement.setString(5, product.getDescription());
            preparedStatement.setDouble(6, product.getCurrentPrice());
            preparedStatement.executeUpdate();
        }
    }

    public void delete(int productId) throws SQLException {
        String deleteProduct = "DELETE FROM shop.public.product WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(deleteProduct);
        preparedStatement.setInt(1, productId);
        preparedStatement.executeUpdate();
    }
}
