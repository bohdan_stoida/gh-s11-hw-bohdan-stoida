package org.geekhub.bohdan.task2.jdbc_stuff;

import org.geekhub.bohdan.task2.shop_stuff.Order;
import org.geekhub.bohdan.task2.shop_stuff.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JDBCOrder {
    private final Connection connection;
    private final List<Order> orders;

    public JDBCOrder(Connection connection, List<Order> orders) {
        this.connection = connection;
        this.orders = orders;
    }

    public void insert() throws SQLException {
        String insertUpdOrder = "INSERT INTO shop.public.order (delivery_place) VALUES (?)"
                + "ON CONFLICT (id) DO UPDATE SET delivery_place = ?;";
        String insertUpdOrderingProduct = "INSERT INTO shop.public.ordering_product (quantity, price) VALUES (?, ?)"
                + "ON CONFLICT (id) DO UPDATE SET quantity = ?, price = ?;";

        for (Order order : orders) {
            PreparedStatement preparedStatementForOrder = connection.prepareStatement(insertUpdOrder);
            preparedStatementForOrder.setString(1, order.getDeliveryPlace());
            preparedStatementForOrder.setString(2, order.getDeliveryPlace());
            preparedStatementForOrder.executeUpdate();

            PreparedStatement preparedStatementForOrderingProduct = connection.prepareStatement(insertUpdOrderingProduct);
            preparedStatementForOrderingProduct.setInt(1, order.getProducts().getQuantity());
            preparedStatementForOrderingProduct.setDouble(2, order.getProducts().getPrice());
            preparedStatementForOrderingProduct.setInt(3, order.getProducts().getQuantity());
            preparedStatementForOrderingProduct.setDouble(4, order.getProducts().getPrice());
            preparedStatementForOrderingProduct.executeUpdate();
        }
    }

    public void delete(int orderId, int orderingProductId) throws SQLException {
        String deleteOrder = "DELETE FROM shop.public.order WHERE id = ?";
        String deleteOrderingProduct = "DELETE FROM shop.public.ordering_product WHERE id = ?";

        PreparedStatement preparedStatementForOrder = connection.prepareStatement(deleteOrder);
        preparedStatementForOrder.setInt(1, orderId);
        preparedStatementForOrder.executeUpdate();
        PreparedStatement preparedStatementForOrderingProduct = connection.prepareStatement(deleteOrderingProduct);
        preparedStatementForOrderingProduct.setInt(1, orderingProductId);
        preparedStatementForOrderingProduct.executeUpdate();
    }

    public Map<Product, Integer> showMostPopularProduct() throws SQLException {
        Map<Product, Integer> mostPopularProduct = new HashMap<>();
        String getMostPopularProduct = "SELECT p.id, p.name, p.description, p.current_price, SUM(op.quantity) AS most_pop_product_quantity " +
                "FROM shop.public.product p " +
                "JOIN shop.public.ordering_product op on p.id = op.product " +
                "GROUP BY p.id " +
                "ORDER BY most_pop_product_quantity DESC LIMIT 1;";

        PreparedStatement preparedStatement = connection.prepareStatement(getMostPopularProduct);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            String name = resultSet.getString("name");
            String description = resultSet.getString("description");
            double currentPrice = resultSet.getDouble("current_price");
            Integer mostPopProductQuantity = resultSet.getInt("most_pop_product_quantity");
            Product product = new Product(name, description, currentPrice);
            mostPopularProduct.put(product, mostPopProductQuantity);
        }

        return mostPopularProduct;
    }
}
