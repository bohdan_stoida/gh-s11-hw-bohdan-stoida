--1.Выбрать информацию по стране с самым большим количеством областей (все поля таблицы стран + количество областей).
SELECT co.id, co.sortname, co.name, co.phonecode, COUNT(s.country_id) AS state_count
FROM countries co
         INNER JOIN states s ON co.id = s.country_id
GROUP BY co.id
ORDER BY state_count DESC
LIMIT 1;

--2.Выбрать информацию по стране с самым большим количеством городов.
SELECT co.id, co.sortname, co.name, co.phonecode, COUNT(c.state_id) AS city_count
FROM countries co
         INNER JOIN states s ON co.id = s.country_id
         INNER JOIN cities c ON s.id = c.state_id
GROUP BY co.id
ORDER BY city_count DESC
LIMIT 1;

--3.Выбрать информацию по всем странам с количеством областей в них.
--Отсортировать по количеству областей в убывающем порядке, названию страны, айдишнику.
SELECT co.id, co.sortname, co.name, co.phonecode, COUNT(s.country_id) AS state_count
FROM countries co
         INNER JOIN states s ON co.id = s.country_id
GROUP BY co.id, co.name
ORDER BY state_count DESC;

--4.Аналогично п. 4 только с количеством городов.
SELECT co.id, co.sortname, co.name, co.phonecode, COUNT(c.state_id) AS city_count
FROM countries co
         INNER JOIN states s ON co.id = s.country_id
         INNER JOIN cities c ON s.id = c.state_id
GROUP BY co.id, co.name
ORDER BY city_count DESC;

--5.Выбрать информацию по всем странам с количеством областей и количеством городов одновременно.
SELECT co.id, co.sortname, co.name, co.phonecode, COUNT(DISTINCT s.id) AS state_count, COUNT(c.id) AS city_count
FROM countries co
         INNER JOIN states s ON co.id = s.country_id
         INNER JOIN cities c ON s.id = c.state_id
GROUP BY co.id;

--6.Вывести информацию о 10-ти странах у которых наибольшее количество городов в какой-либо области.
SELECT *
FROM (SELECT DISTINCT ON (co.id) co.id, co.sortname, co.name, co.phonecode, COUNT(c.id) AS city_count
      FROM countries co
               JOIN states s on co.id = s.country_id
               JOIN cities c on s.id = c.state_id
      GROUP BY co.id, s.id
      ORDER BY co.id, city_count DESC) AS new_table
ORDER BY new_table.city_count DESC
LIMIT 10;

--7.В одном запросе выбрать информацию отсортированную в алфавитном порядке по названию страны,
--по первым десяти и последним десяти странам с наибольшим количеством областей.
--(То есть 10 у кого областей больше всего и 10 у кого областей меньше всего).
(SELECT co.id, co.sortname, co.name, co.phonecode, COUNT(s.id) AS state_count
 FROM countries co
          INNER JOIN states s on co.id = s.country_id
 GROUP BY co.id, co.name
 ORDER BY state_count DESC
 LIMIT 10)
UNION
(SELECT co.id, co.sortname, co.name, co.phonecode, COUNT(s.id) AS state_count
 FROM countries co
          INNER JOIN states s on co.id = s.country_id
 GROUP BY co.id, co.name
 ORDER BY state_count
 LIMIT 10);

--8.Выбрать все страны у которых количество областей больше, чем среднее количество областей по всем странам.
SELECT co.id, co.sortname, co.name, co.phonecode
FROM countries co
         INNER JOIN (SELECT s.country_id,
                            COUNT(*)              AS state_count,
                            AVG(COUNT(*)) OVER () AS avg_state_count
                     FROM states s
                     GROUP BY s.country_id)
    AS tmp_table on tmp_table.country_id = co.id
GROUP BY co.id, tmp_table.state_count, tmp_table.avg_state_count
HAVING tmp_table.state_count > tmp_table.avg_state_count;

--9.Выбрать информацию по одной стране на каждое значение количества областей.
--(То есть если есть 10 стран с количеством областей == 1, то берем первую по алфавиту страну,
--если есть 30 стран у которых 2 области, то тоже берем первую по алфавиту и т.д.).
--Получится список типа: где крайняя справа колонка - это количество областей, остальное - информация про страну.
SELECT DISTINCT ON (tmp_table.state_count) *
FROM (SELECT co.id, co.sortname, co.name, co.phonecode, COUNT(s.id) AS state_count
      FROM countries co
               JOIN states s on co.id = s.country_id
      GROUP BY co.id, co.name
      ORDER BY state_count, co.name) AS tmp_table;

--10.Найти информацию по всем дублирующимся областям (сравнивать по названию).
SELECT s.id, s.name, s.country_id
FROM countries co
         INNER JOIN states s ON co.id = s.country_id
WHERE co.name = s.name;

--11.Найти области у которых нет городов.
SELECT s.id, s.name, s.country_id
FROM states s
         INNER JOIN cities c on s.id = c.state_id
WHERE c.name IS NULL;