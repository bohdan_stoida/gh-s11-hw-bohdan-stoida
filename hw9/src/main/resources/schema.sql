CREATE TABLE customer
(
    "id"       BIGSERIAL   NOT NULL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name  VARCHAR(50) NOT NULL,
    cell_phone VARCHAR(50) NOT NULL
);

CREATE TABLE product
(
    "id"            BIGSERIAL     NOT NULL PRIMARY KEY,
    "name"          VARCHAR(50)   NOT NULL,
    "description"   VARCHAR(150)  NOT NULL,
    "current_price" NUMERIC(3, 2) NOT NULL
);

CREATE TABLE "order"
(
    "id"             BIGSERIAL    NOT NULL PRIMARY KEY,
    "customer"       BIGSERIAL    NOT NULL REFERENCES customer (id),
    "delivery_place" VARCHAR(150) NOT NULL
);

CREATE TABLE ordering_product
(
    "id"       BIGSERIAL     NOT NULL PRIMARY KEY,
    "product"  BIGSERIAL     NOT NULL REFERENCES product (id),
    "order"    BIGSERIAL     NOT NULL REFERENCES "order" (id),
    "quantity" INTEGER       NOT NULL,
    "price"    NUMERIC(3, 2) NOT NULL
);