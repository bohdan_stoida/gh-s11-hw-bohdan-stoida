package org.geekhub.bohdan;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {

        HttpReader httpReader = new HttpReader();
        httpReader.readLinks();
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        for (String url : httpReader.getUrls()) {
            executorService.submit(new MD5HashWriter(url));
        }
        executorService.shutdown();
    }
}
