package org.geekhub.bohdan;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5HashWriter implements Runnable {
    private final String url;

    public MD5HashWriter(String url) {
        this.url = url;
    }

    @Override
    public void run() {
        try (FileOutputStream fos = new FileOutputStream("./hw11/src/main/resources/result.txt", true);
             InputStream inputStream = new URL(this.url).openStream()) {
            MessageDigest messageDigest = null;
            try {
                messageDigest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            String digest = getMd5(inputStream, messageDigest);
            System.out.println("URL: " + url + " - MD5: " + digest);

            String textResult = "URL: " + url + " - MD5: " + digest + "\n";
            byte[] buffer = textResult.getBytes();
            fos.write(buffer, 0, buffer.length);
            System.out.println("Result of MD5 hash written!");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private String getMd5(InputStream inputStream, MessageDigest md) {
        md.reset();
        byte[] bytes = new byte[2048];
        int numBytes = 0;
        while (true) {
            try {
                if ((numBytes = inputStream.read(bytes)) == -1) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            md.update(bytes, 0, numBytes);
        }
        byte[] digest = md.digest();
        StringBuilder result = new StringBuilder();
        for (byte b : digest) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }
}
