package org.geekhub.bohdan;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpReader {

    private final List<String> urls = new ArrayList<>();

    public List<String> getUrls() {
        return urls;
    }

    public HttpReader() {
    }

    public void readLinks() {
        try (BufferedReader br = new BufferedReader(new FileReader("./hw11/src/main/resources/http_links.txt"))) {
            String line = br.readLine();
            while (line != null) {
                urls.add(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
