package org.geekhub.bohdan.task1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InvalidShapeException {

        Shape shape = null;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Type shape name (Triangle, Rectangle, Square or Circle): ");
        String nameOfShape = scanner.nextLine();
        switch (nameOfShape) {
            case "Triangle":
                System.out.println("Enter sides of the triangle:");
                System.out.print("a = ");
                float a = scanner.nextFloat();
                System.out.print("b = ");
                float b = scanner.nextFloat();
                System.out.print("c = ");
                float c = scanner.nextFloat();
                if (b + c < a || a + c < b || a + b < c && a <= 0 || b <= 0 || c <= 0)
                    throw new InvalidShapeException("Triangle doesn't exist!");
                shape = new Triangle(a, b, c);
                break;
            case "Rectangle":
                System.out.println("Enter sides of the rectangle:");
                System.out.print("First side = ");
                a = scanner.nextFloat();
                System.out.print("Second side = ");
                b = scanner.nextFloat();
                if (a < 0 || b < 0)
                    throw new InvalidShapeException("Rectangle doesn't exist!");
                shape = new Rectangle(a, b);
                break;
            case "Square":
                System.out.println("Enter side of the square:");
                System.out.print("Side length = ");
                a = scanner.nextFloat();
                if (a < 0)
                    throw new InvalidShapeException("Square doesn't exist!");
                shape = new Square(a);
                break;
            case "Circle":
                System.out.println("Enter radius of the circle:");
                System.out.print("Radius = ");
                float radius = scanner.nextFloat();
                if (radius < 0)
                    throw new InvalidShapeException("Circle doesn't exist!");
                shape = new Circle(radius);
                break;
            default:
                System.out.println("Invalid data!");
                break;
        }

        if (shape != null) {
            System.out.println(shape.getClass().getSimpleName() + " area: " + shape.calculateArea() + "\n"
                    + shape.getClass().getSimpleName() + " perimeter: " + shape.calculatePerimeter());
            if (shape instanceof Rectangle) {
                Rectangle rectangle = (Rectangle) shape;
                System.out.println("Properties of the triangles:");
                System.out.print("a = " + rectangle.getFirstSide() + "\n");
                System.out.print("b = " + rectangle.getSecondSide() + "\n");
                System.out.print("c = " + rectangle.calculateDiagonal());
            }
            if (shape instanceof Square) {
                Square square = (Square) shape;
                System.out.println("Properties of the triangles:");
                System.out.print("a = " + square.getSideLength() + "\n");
                System.out.print("b = " + square.getSideLength() + "\n");
                System.out.print("c = " + square.calculateDiagonal());
            }
        }
    }
}
