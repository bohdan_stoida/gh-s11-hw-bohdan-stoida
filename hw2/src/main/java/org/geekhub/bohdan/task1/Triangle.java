package org.geekhub.bohdan.task1;

public class Triangle extends Shape {

    private final float a;
    private final float b;
    private final float c;

    protected Triangle(float a, float b, float c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public float calculateArea() {
        float halfPerimeter = calculatePerimeter() / 2;
        return (float) Math.sqrt(halfPerimeter * (halfPerimeter - a) *
                (halfPerimeter - b) * (halfPerimeter - c));
    }

    @Override
    public float calculatePerimeter() {
        return a + b + c;
    }
}
