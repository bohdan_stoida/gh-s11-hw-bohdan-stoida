package org.geekhub.bohdan.task1;

public abstract class Shape {

    abstract public float calculateArea();

    abstract public float calculatePerimeter();
}
