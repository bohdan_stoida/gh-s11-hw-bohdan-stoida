package org.geekhub.bohdan.task2;

public interface IShape {

    float calculateArea();

    float calculatePerimeter();
}
