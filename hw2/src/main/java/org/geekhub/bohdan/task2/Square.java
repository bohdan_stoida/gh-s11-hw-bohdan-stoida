package org.geekhub.bohdan.task2;

public class Square implements IShape {

    private final float sideLength;

    protected Square(float sideLength) {
        this.sideLength = sideLength;
    }

    public float getSideLength() {
        return sideLength;
    }

    public float calculateDiagonal() {
        return (float) Math.sqrt(2) * sideLength;
    }

    @Override
    public float calculateArea() {
        return (float) Math.pow(sideLength, 2);
    }

    @Override
    public float calculatePerimeter() {
        return 4 * sideLength;
    }
}
