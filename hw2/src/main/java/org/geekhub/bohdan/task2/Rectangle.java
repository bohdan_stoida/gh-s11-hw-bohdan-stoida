package org.geekhub.bohdan.task2;

public class Rectangle implements IShape {

    private final float firstSide;
    private final float secondSide;

    protected Rectangle(float firstSide, float secondSide) {
        this.firstSide = firstSide;
        this.secondSide = secondSide;
    }

    public float getFirstSide() {
        return firstSide;
    }

    public float getSecondSide() {
        return secondSide;
    }

    public float calculateDiagonal() {
        return (float) Math.sqrt(Math.pow(firstSide, 2) + Math.pow(secondSide, 2));
    }

    @Override
    public float calculateArea() {
        return firstSide * secondSide;
    }

    @Override
    public float calculatePerimeter() {
        return 2 * (firstSide + secondSide);
    }
}
