package org.geekhub.bohdan.task2;

public class Circle implements IShape {

    private final float radius;

    protected Circle(float radius) {
        this.radius = radius;
    }

    @Override
    public float calculateArea() {
        return (float) (Math.PI * Math.pow(radius, 2));
    }

    @Override
    public float calculatePerimeter() {
        return 2 * (float) Math.PI * radius;
    }
}
