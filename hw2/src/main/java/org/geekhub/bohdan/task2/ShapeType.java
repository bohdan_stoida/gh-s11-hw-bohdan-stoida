package org.geekhub.bohdan.task2;

public enum ShapeType {
    CIRCLE,
    RECTANGLE,
    SQUARE,
    TRIANGLE
}
