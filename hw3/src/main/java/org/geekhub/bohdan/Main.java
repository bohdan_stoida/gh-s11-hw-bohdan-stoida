package org.geekhub.bohdan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int wordLength = scanner.nextInt();
        String[] words = new String[wordLength];
        char[] wordChars;

        System.out.println("Input:");
        for (int i = 0; i < words.length; i++) {
            String word = scanner.next();
            words[i] = word;
        }

        System.out.println("Output:");
        for (String s : words) {
            if (s.length() <= wordLength) {
                System.out.println(s);
            } else {
                wordChars = s.toCharArray();
                char first = wordChars[0];
                char last = wordChars[wordChars.length - 1];
                System.out.print(first);
                System.out.print(wordChars.length - 2);
                System.out.print(last);
                System.out.println();
            }
        }
    }
}
