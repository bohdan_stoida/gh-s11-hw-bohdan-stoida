package org.geekhub.bohdan.task1;

public abstract class Product {

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    protected float price;
    protected String name;
    protected int quantity;

    public Product(float price, String name, int quantity) {
        this.price = price;
        this.name = name;
        this.quantity = quantity;
    }
}
