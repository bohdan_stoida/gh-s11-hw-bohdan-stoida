package org.geekhub.bohdan.task1;

import java.util.ArrayList;
import java.util.List;

public class Inventory {

    private final List<List<Product>> productList = new ArrayList<>();

    public void addProducts(List<? extends Product> product) {
        productList.add((List<Product>) product);
    }

    public float sumUpInventoryValue() {
        float sum = 0;

        for (List<Product> products : productList) {
            for (Product product : products) {
                sum += product.price;
            }
        }

        return sum;
    }
}
