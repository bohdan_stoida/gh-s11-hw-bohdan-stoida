package org.geekhub.bohdan.task1;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Inventory inventory = new Inventory();

        List<Vegetables> vegetablesList = new ArrayList<>();
        vegetablesList.add(new Carrot(100, "Orange carrot", 2000));
        vegetablesList.add(new Onion(222, "Delicious onion", 1433));
        vegetablesList.add(new Potato(300, "Just potato", 10000));
        List<JunkFood> junkFoodList = new ArrayList<>();
        junkFoodList.add(new Burger(34, "double cheeseburger", 10000));
        junkFoodList.add(new Pizza(54, "Margarita", 53));
        junkFoodList.add(new Chips(56, "Pringles", 1000000));
        List<Fruits> fruitsList = new ArrayList<>();
        fruitsList.add(new Apple(22, "sweet apple", 543));
        fruitsList.add(new Banana(44, "yellow banana", 244));
        fruitsList.add(new Orange(122, "orange orange", 345));

        inventory.addProducts(vegetablesList);
        inventory.addProducts(junkFoodList);
        inventory.addProducts(fruitsList);

        System.out.println("Inventory cost: " + inventory.sumUpInventoryValue() + " UAH");
    }
}
