package org.geekhub.bohdan.task2;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private final List<Teacher> teachers;
    private List<Student> students;

    public Group() {
        teachers = new ArrayList<>();
        students = new ArrayList<>();
    }

    public void addTeachers(Teacher teacher) {
        teachers.add(teacher);
    }

    public void addStudents(Student student) {
        students.add(student);
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
