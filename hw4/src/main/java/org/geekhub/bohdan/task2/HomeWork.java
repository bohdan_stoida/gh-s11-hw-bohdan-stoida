package org.geekhub.bohdan.task2;

public class HomeWork {

    private String name;

    public HomeWork(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
