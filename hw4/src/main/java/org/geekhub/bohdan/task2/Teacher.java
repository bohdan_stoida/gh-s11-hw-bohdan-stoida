package org.geekhub.bohdan.task2;

import java.util.ArrayList;
import java.util.Random;

public class Teacher {

    private String name;
    private ArrayList<Float> assignedGrades;
    private int moreFives;
    Random random;

    public Teacher(String name) {
        this.name = name;
        assignedGrades = new ArrayList<>();
        random = new Random();
    }

    public String getName() {
        return name;
    }

    public float setMark() {
        return random.nextInt(6);
    }

    public void setMark(float mark) {
        assignedGrades.add(mark);
    }

    public ArrayList<Float> getAssignedGrades() {
        return assignedGrades;
    }

    public int getMoreFives() {
        return moreFives;
    }

    public void setMoreFives(int moreFives) {
        this.moreFives = moreFives;
    }
}
