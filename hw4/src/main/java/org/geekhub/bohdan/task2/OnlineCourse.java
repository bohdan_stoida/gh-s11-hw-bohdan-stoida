package org.geekhub.bohdan.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class OnlineCourse {

    private Group group;
    private ArrayList<Lecture> lectures;
    private ArrayList<HomeWork> homeWorks;
    private int homeWorkIndex = 0;
    private int inTimeCount;
    private int isPresentCount;
    Random random = new Random();

    public OnlineCourse(Group group) {
        this.group = group;
        lectures = new ArrayList<>();
        homeWorks = new ArrayList<>();
    }

    public void runStudyingProcess() {
        System.out.println("-------------------");
        System.out.println("Start of lecture #" + homeWorkIndex);
        for (Student student : group.getStudents()) {
            boolean studentIsPresent = random.nextBoolean();
            boolean inTime = random.nextBoolean();
            int randomTeacher = random.nextInt(2);
            if (studentIsPresent) {
                student.setPresent(studentIsPresent);
                isPresentCount = student.getNumberOfAttendedClasses();
                student.setNumberOfAttendedClasses(++isPresentCount);
                System.out.println(student.getName() + " is present (" + studentIsPresent + ")");
                System.out.println(student.getName() + " attended " + student.getNumberOfAttendedClasses() + " classes");
                student.setHomeWork(homeWorks.get(homeWorkIndex));
                System.out.println(student.getName() + " got " + student.getHomeWork().getName());
                student.setInTime(inTime);
                if (inTime) {
                    inTimeCount = student.getNumberOfCompletedOnTimeHomeWorks();
                    student.setNumberOfCompletedOnTimeHomeWorks(++inTimeCount);
                    System.out.println(student.getName() + " made home work in time (" + inTime + ")");
                    System.out.println(student.getName() + " made home work in time " + student.getNumberOfCompletedOnTimeHomeWorks() + " times\n");
                } else {
                    System.out.println(student.getName() + " made home work not in time (" + inTime + ")\n");
                }
                float mark = group.getTeachers().get(randomTeacher).setMark();
                student.setMark(mark);
                group.getTeachers().get(randomTeacher).setMark(mark);
            } else {
                student.setMissedLecture(lectures.get(homeWorkIndex));
                student.setPresent(studentIsPresent);
                System.out.println(student.getName() + " is not present (" + studentIsPresent + ")\n");
            }
        }

        homeWorkIndex++;
    }

    public void deleteStudents() {
        System.out.println("Before:");
        System.out.println(group.getStudents());
        List<Student> studentsToDelete = new ArrayList<>();
        float sumOfMarks = 0;
        for (Student student : group.getStudents()) {
            for (float mark : student.getMarks()) {
                sumOfMarks += mark;
            }
            float averageMark = sumOfMarks / student.getMarks().size();
            if (averageMark < 3.5f || student.getNumberOfCompletedOnTimeHomeWorks() < Math.random() * 0.5)
                System.out.println(student.getName() + " removed");
            else
                studentsToDelete.add(student);
            sumOfMarks = 0;
        }
        group.setStudents(studentsToDelete);
        System.out.println("After:");
        System.out.println(group.getStudents());
        if(group.getStudents().isEmpty())
            System.out.println("Keep running the application...");
    }

    public void findTruantStudent() {
        if (group.getStudents().size() != 0) {
            int maxElement = group.getStudents().get(0).getNumberOfAttendedClasses();
            Student mostTruantStudent = group.getStudents().get(0);
            for (int i = 0; i < group.getStudents().size(); i++) {
                boolean studentIsFound = false;
                if (maxElement < group.getStudents().get(i).getNumberOfAttendedClasses()) {
                    maxElement = group.getStudents().get(i).getNumberOfAttendedClasses();
                    studentIsFound = true;
                }
                if (studentIsFound)
                    mostTruantStudent = group.getStudents().get(i);
            }
            int maxNumberOfAttendedClasses = 5;
            int skippedLectures = maxNumberOfAttendedClasses - maxElement;
            System.out.println(mostTruantStudent.getName() + " skipped " + skippedLectures + " lectures");
            for (Lecture lecture : mostTruantStudent.getMissedLectures())
                System.out.println("Lecture questioning - " + lecture.getName());
        } else {
            System.out.println("All students were deleted!");
        }
    }

    public void findTeacher() {
        int counter = 0;
        Teacher betterTeacher;
        for (Teacher teacher : group.getTeachers()) {
            for (float mark : teacher.getAssignedGrades()) {
                if (mark == 5) {
                    teacher.setMoreFives(counter++);
                }
            }
            counter = 0;
        }
        if (group.getTeachers().get(0).getMoreFives() > group.getTeachers().get(1).getMoreFives())
            betterTeacher = group.getTeachers().get(0);
        else
            betterTeacher = group.getTeachers().get(1);
        if (betterTeacher != null)
            System.out.println(betterTeacher.getName() + " put more fives");
    }

    public void addLecture(Lecture lecture) {
        lectures.add(lecture);
    }

    public void addHomeWork(HomeWork homeWork) {
        homeWorks.add(homeWork);
    }
}
