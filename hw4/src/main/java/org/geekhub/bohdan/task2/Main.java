package org.geekhub.bohdan.task2;

public class Main {

    public static void main(String[] args) {

        Group group = new Group();
        for (int i = 0; i < 2; i++)
            group.addTeachers(new Teacher("Teacher" + i));
        for (int i = 0; i < 5; i++)
            group.addStudents(new Student("Student" + i, false, 0, false, 0, null));

        OnlineCourse onlineCourse = new OnlineCourse(group);
        for (int i = 0; i < 5; i++) {
            onlineCourse.addLecture(new Lecture("Lecture" + i));
            onlineCourse.addHomeWork(new HomeWork("HomeWork" + i));
        }
        for (int i = 0; i < 5; i++)
            onlineCourse.runStudyingProcess();
        onlineCourse.deleteStudents();
        onlineCourse.findTruantStudent();
        onlineCourse.findTeacher();
    }
}
