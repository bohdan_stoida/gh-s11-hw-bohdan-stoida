package org.geekhub.bohdan.task2;

import java.util.ArrayList;

public class Student {

    private String name;
    private ArrayList<Float> marks;
    private ArrayList<Lecture> missedLectures;
    private boolean isPresent;
    private int numberOfAttendedClasses;
    private boolean inTime;
    private int numberOfCompletedOnTimeHomeWorks;
    private HomeWork homeWork;

    public Student(String name, boolean isPresent, int numberOfAttendedClasses, boolean inTime, int numberOfCompletedOnTimeHomeWorks, HomeWork homeWork) {
        this.name = name;
        this.numberOfAttendedClasses = numberOfAttendedClasses;
        this.numberOfCompletedOnTimeHomeWorks = numberOfCompletedOnTimeHomeWorks;
        marks = new ArrayList<>();
        missedLectures = new ArrayList<>();
        this.isPresent = isPresent;
        this.inTime = inTime;
        this.homeWork = homeWork;
    }

    public String getName() {
        return name;
    }

    public void setPresent(boolean present) {
        isPresent = present;
    }

    public ArrayList<Float> getMarks() {
        return marks;
    }

    public void setMark(float mark) {
        marks.add(mark);
    }

    public ArrayList<Lecture> getMissedLectures() {
        return missedLectures;
    }

    public void setMissedLecture(Lecture lecture) {
        missedLectures.add(lecture);
    }

    public HomeWork getHomeWork() {
        return homeWork;
    }

    public void setHomeWork(HomeWork homeWork) {
        this.homeWork = homeWork;
    }

    public void setInTime(boolean inTime) {
        this.inTime = inTime;
    }

    public int getNumberOfAttendedClasses() {
        return numberOfAttendedClasses;
    }

    public void setNumberOfAttendedClasses(int numberOfAttendedClasses) {
        this.numberOfAttendedClasses = numberOfAttendedClasses;
    }

    public int getNumberOfCompletedOnTimeHomeWorks() {
        return numberOfCompletedOnTimeHomeWorks;
    }

    public void setNumberOfCompletedOnTimeHomeWorks(int numberOfCompletedOnTimeHomeWorks) {
        this.numberOfCompletedOnTimeHomeWorks = numberOfCompletedOnTimeHomeWorks;
    }
}
