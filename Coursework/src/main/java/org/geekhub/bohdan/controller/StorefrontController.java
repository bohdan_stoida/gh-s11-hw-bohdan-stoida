package org.geekhub.bohdan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class StorefrontController {

    @GetMapping()
    public String storefront() {
        return "storefront";
    }
}