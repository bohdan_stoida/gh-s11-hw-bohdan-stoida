package org.geekhub.bohdan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NFTMarketplaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(NFTMarketplaceApplication.class, args);
    }
}
