package org.geekhub.bohdan.task2;

public class A {

    private int height;
    private String gender;
    private int age;
    private int weight;
    private B b;
    private C c;

    public A(int height, String gender, int age, int weight) {
        this.height = height;
        this.gender = gender;
        this.age = age;
        this.weight = weight;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    public C getC() {
        return c;
    }

    public void setC(C c) {
        this.c = c;
    }

    @Override
    public String toString() {
        return "A{" +
                "height=" + height +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}
