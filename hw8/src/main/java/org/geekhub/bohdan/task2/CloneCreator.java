package org.geekhub.bohdan.task2;

import java.lang.reflect.InvocationTargetException;

public class CloneCreator {

    public CloneCreator() {
    }

    public Object cloneA(Object obj) {
        Class[] clazzArgs = new Class[4];
        clazzArgs[0] = int.class;
        clazzArgs[1] = String.class;
        clazzArgs[2] = int.class;
        clazzArgs[3] = int.class;

        try {
            return obj.getClass().getDeclaredConstructor(clazzArgs).newInstance(179, "Male", 22, 75);
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return null;
    }
}
