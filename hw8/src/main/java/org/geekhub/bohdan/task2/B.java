package org.geekhub.bohdan.task2;

public class B {

    private String color;
    private int maxSpeed;
    private String type;
    private String model;

    public B(String color, int maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
    }

    @Override
    public String toString() {
        return "B{" +
                "color='" + color + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
