package org.geekhub.bohdan.task2;

public class C {

    private String color;
    private int age;
    private int legCount;
    private int fullLength;

    public C(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }

    @Override
    public String toString() {
        return "C{" +
                "color='" + color + '\'' +
                ", age=" + age +
                ", legCount=" + legCount +
                ", fullLength=" + fullLength +
                '}';
    }
}
