package org.geekhub.bohdan.task2;

public class Main {

    public static void main(String[] args) {

        A a = new A(180, "male", 22, 75);
        B b = new B("Black", 190, "Sedan", "RX-7");
        C c = new C("Black", 3, 4, 35);
        a.setB(b);
        a.setC(c);

        System.out.println("Before cloning:");
        System.out.println(a);

        CloneCreator cloneCreator = new CloneCreator();
        A clonedA = (A) cloneCreator.cloneA(a);
        clonedA.setB(b);
        clonedA.setC(c);

        System.out.println("After cloning:");
        System.out.println(clonedA);
    }
}
