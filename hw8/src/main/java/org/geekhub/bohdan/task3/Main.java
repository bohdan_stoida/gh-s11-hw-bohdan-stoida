package org.geekhub.bohdan.task3;

public class Main {

    public static void main(String[] args) {

        Car a = new Car(120, "RX-7", "coupe", "black", 230);
        Car b = new Car(120, "RX-8", "sedan", "black", 225);

        BeanComparator beanComparator = new BeanComparator(a, b);
        beanComparator.compareBeans();
    }
}
