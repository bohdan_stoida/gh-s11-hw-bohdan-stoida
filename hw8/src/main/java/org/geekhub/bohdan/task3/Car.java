package org.geekhub.bohdan.task3;

import java.util.Objects;

public class Car {

    @Ignore
    private int maxCount;
    private String model;
    private String type;
    private String color;
    @Ignore
    private int maxSpeed;

    public Car(int maxCount, String model, String type, String color, int maxSpeed) {
        this.maxCount = maxCount;
        this.model = model;
        this.type = type;
        this.color = color;
        this.maxSpeed = maxSpeed;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return maxCount == car.maxCount && maxSpeed == car.maxSpeed && Objects.equals(model, car.model) && Objects.equals(type, car.type) && Objects.equals(color, car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maxCount, model, type, color, maxSpeed);
    }
}
