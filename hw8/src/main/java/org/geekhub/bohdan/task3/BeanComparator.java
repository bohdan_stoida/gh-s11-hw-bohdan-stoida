package org.geekhub.bohdan.task3;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BeanComparator {

    private final Car car1;
    private final Car car2;
    private boolean match;

    public BeanComparator(Car car1, Car car2) {
        this.car1 = car1;
        this.car2 = car2;
    }

    public void compareBeans() {
        Class<? extends Car> carClazz1 = car1.getClass();
        Class<? extends Car> carClazz2 = car2.getClass();

        Field firstFieldFromFirstClass = carClazz1.getDeclaredFields()[0];
        Field lastFieldFromFirstClass = carClazz1.getDeclaredFields()[4];
        Field firstFieldFromSecondClass = carClazz1.getDeclaredFields()[0];
        Field lastFieldFromSecondClass = carClazz1.getDeclaredFields()[4];

        compareIntegerValues(firstFieldFromFirstClass, firstFieldFromSecondClass, compareMaxCount);

        Field[] car2Fields = carClazz2.getDeclaredFields();
        List<Field> car2FieldList = Arrays.stream(car2Fields).filter(field -> !field.isAnnotationPresent(Ignore.class))
                .peek(field -> field.setAccessible(true))
                .collect(Collectors.toList());
        int index = 0;
        for (Field declaredField : carClazz1.getDeclaredFields()) {
            if (declaredField.isAnnotationPresent(Ignore.class)) {
                continue;
            } else {
                declaredField.setAccessible(true);
                Object car1FieldValue = null;
                Object car2FieldValue = null;
                try {
                    car1FieldValue = declaredField.get(car1);
                    car2FieldValue = car2FieldList.get(index++).get(car2);
                } catch (IllegalAccessException e) {
                    System.out.println(e.getMessage());
                    System.out.println("Stack trace:");
                    e.printStackTrace();
                }
                if (car1FieldValue != null) {
                    match = car1FieldValue.equals(car2FieldValue);
                }
                System.out.println(declaredField.getName() + "\t" + car1FieldValue + "\t" + car2FieldValue + "\t" + match);
            }
        }

        compareIntegerValues(lastFieldFromFirstClass, lastFieldFromSecondClass, compareMaxSpeed);
    }

    private void compareIntegerValues(Field fieldFromFirstClass, Field fieldFromSecondClass, Comparator<Car> carComparator) {
        try {
            carComparator.compare(car1, car2);
            fieldFromFirstClass.setAccessible(true);
            fieldFromSecondClass.setAccessible(true);
            System.out.println(fieldFromFirstClass.getName() + "\t" + fieldFromFirstClass.get(car1) + "\t"
                    + fieldFromSecondClass.get(car2) + "\t" + match);
            fieldFromFirstClass.setAccessible(false);
            fieldFromSecondClass.setAccessible(false);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private final Comparator<Car> compareMaxCount = (o1, o2) -> {
        int maxCount = Integer.compare(o1.getMaxCount(), o2.getMaxCount());
        if (maxCount != 0) {
            match = false;
            return maxCount;
        } else {
            match = true;
        }

        return 0;
    };

    private final Comparator<Car> compareMaxSpeed = (o1, o2) -> {
        int maxSpeed = Integer.compare(o1.getMaxSpeed(), o2.getMaxSpeed());
        if (maxSpeed != 0) {
            match = false;
            return maxSpeed;
        } else {
            match = true;
        }

        return 0;
    };
}
