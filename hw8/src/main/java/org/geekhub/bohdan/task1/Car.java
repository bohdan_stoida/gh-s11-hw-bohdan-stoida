package org.geekhub.bohdan.task1;

public class Car {

    private Color color;
    private int maxSpeed;
    private String type;
    private String model;

    public Car(Color color, int maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
    }
}
