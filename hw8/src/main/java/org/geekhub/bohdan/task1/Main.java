package org.geekhub.bohdan.task1;

public class Main {

    public static void main(String[] args) {

        Cat cat = new Cat(Color.BLACK, 3, 4, 35);
        Car car = new Car(Color.BLACK, 190, "Sedan", "RX-7");
        Human human = new Human(180, "male", 22, 75);
        BeanRepresenter beanRepresenter = new BeanRepresenter(cat, car, human);

        beanRepresenter.representBean(cat);
        beanRepresenter.representBean(car);
        beanRepresenter.representBean(human);
    }
}
