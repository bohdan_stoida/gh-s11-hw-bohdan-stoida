package org.geekhub.bohdan.task1;

import java.lang.reflect.Field;

public class BeanRepresenter {

    private final Cat cat;
    private final Car car;
    private final Human human;

    public BeanRepresenter(Cat cat, Car car, Human human) {
        this.cat = cat;
        this.car = car;
        this.human = human;
    }

    public void representBean(Object obj) {
        Class<? extends Object> clazz = obj.getClass();
        System.out.println(clazz.getSimpleName());
        for (Field declaredField : clazz.getDeclaredFields()) {
            if (!declaredField.isAccessible()) {
                declaredField.setAccessible(true);
                try {
                    System.out.println(declaredField.getType() + "\t" + declaredField.getName() + ": " + declaredField.get(obj));
                } catch (IllegalAccessException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        System.out.println();
    }
}
