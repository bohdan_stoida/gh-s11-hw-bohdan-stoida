package org.geekhub.bohdan.task1;

public class Cat {

    private Color color;
    private int age;
    private int legCount;
    private int fullLength;

    public Cat(Color color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }
}
