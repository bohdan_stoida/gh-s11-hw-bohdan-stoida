package org.geekhub.bohdan;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String errorMsg = "Phone number is incorrect.";
        int nationalPhoneNumberLength = 10;
        int internationalPhoneNumberLength = 14;
        boolean programIsOpen = true;
        String phoneNumber = null;
        while (programIsOpen) {
            boolean isCorrectSign = false;
            System.out.print("Please enter the phone number: ");
            phoneNumber = scanner.nextLine();

            if (phoneNumber.length() >= nationalPhoneNumberLength && phoneNumber.length() <= internationalPhoneNumberLength) {
                if (phoneNumber.startsWith("093") || phoneNumber.startsWith("067") || phoneNumber.startsWith("8")
                        || phoneNumber.startsWith("+380") || phoneNumber.startsWith("00380")) {
                    for (int i = 0; i < phoneNumber.length(); i++) {
                        if (Character.isDigit(phoneNumber.charAt(i))) {
                            programIsOpen = false;
                        } else {
                            if (phoneNumber.charAt(i) == '+') {
                                if (phoneNumber.charAt(0) != '+' || isCorrectSign) {
                                    System.out.print(errorMsg);
                                    programIsOpen = true;
                                    break;
                                }
                                isCorrectSign = true;
                            } else {
                                System.out.print(errorMsg);
                                programIsOpen = true;
                                break;
                            }
                        }
                    }
                } else {
                    System.out.print(errorMsg);
                }
            } else {
                System.out.print(errorMsg);
            }

            if (programIsOpen == false)
                System.out.println("Phone number is correct!");
        }

        System.out.println("\nCalculation part");
        int sumOfDigits = 0;
        int iter = 1;
        sumOfDigits = sumOfDigits(phoneNumber, sumOfDigits);
        System.out.println("Sum of all digits of the phone number: " + sumOfDigits);
        while (sumOfDigits >= 10) {
            sumOfDigits = sumOfDigits / 10 + sumOfDigits % 10;
            System.out.println("Sum of the result (step #" + iter + "): " + sumOfDigits);
            iter++;
        }

        switch (sumOfDigits) {
            case 1:
                System.out.println("Final result is: One");
                break;
            case 2:
                System.out.println("Final result is: Two");
                break;
            case 3:
                System.out.println("Final result is: Three");
                break;
            case 4:
                System.out.println("Final result is: Four");
                break;
            default:
                System.out.println("Final result is: " + sumOfDigits);
                break;
        }

        System.out.println("\nSuccess!");
    }

    private static int sumOfDigits(String phoneNumber, int result) {
        for (int i = 0; i < phoneNumber.length(); i++) {
            if (phoneNumber.charAt(i) == '+')
                continue;
            else
                result += Integer.parseInt(String.valueOf(phoneNumber.charAt(i)));
        }

        return result;
    }
}
