package org.geekhub.bohdan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserFeedbackController {

    @GetMapping("/")
    public String index() {
        return "index";
    }
}
