package org.geekhub.bohdan.storage;

public class StorageException extends Exception {

    public StorageException(Throwable cause) {
        super(cause);
    }
}