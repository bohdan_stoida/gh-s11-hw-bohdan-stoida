CREATE TABLE IF NOT EXISTS "database_storage"
(
    "id"         SERIAL       NOT NULL,
    "first_name" VARCHAR(150) NOT NULL,
    "last_name"  VARCHAR(150) NOT NULL,
    "cell_phone" VARCHAR(150) NOT NULL,
    PRIMARY KEY ("id")
);